Install
=======

A shorthand for `yum install`.

Dependencies
------------

- no GNU CoreUtils installed, because /usr/bin/install conflicts.

Downloading
-----------

    git clone git@github.com:bigben87/install.git

Installing
----------

    sudo ln -s `pwd`/install/install.sh /usr/bin/install

Running
-------

    sudo install